function showBack() {
    'use strict';

    var card = document.getElementById('swipe');
    var toggle = card.getAttribute('show-back') == 'true' ? 'false' : 'true';
    card.setAttribute('show-back', toggle);

}